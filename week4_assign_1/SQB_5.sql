SELECT 
    actor_info.first_name,
    actor_info.last_name,
    film.description,
    film.release_yearx
FROM
    film_actor
        INNER JOIN
    actor_info ON film_actor.actor_id = actor_info.actor_id
        INNER JOIN
    film ON film_actor.film_id = film.film_id
WHERE
    film.description LIKE '%Shark%'
        AND description LIKE '%Crocodile%'
ORDER BY actor_info.last_name;