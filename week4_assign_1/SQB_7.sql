SELECT 
    *
FROM
    (SELECT 
        first_name, last_name
    FROM
        actor
    WHERE
        actor_id <> 8 UNION SELECT 
        first_name, last_name
    FROM
        customer) AS S
WHERE
    S.first_name = (SELECT 
            first_name
        FROM
            actor
        WHERE
            actor_id = 8);