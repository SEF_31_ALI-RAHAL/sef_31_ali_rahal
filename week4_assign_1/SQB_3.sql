
SELECT 
    country, COUNT(ID)
FROM
    customer_list
GROUP BY country
ORDER BY COUNT(ID) DESC
LIMIT 3;