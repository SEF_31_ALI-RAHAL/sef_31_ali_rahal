SELECT 
    store_id,
    MONTH(p.payment_date),
    YEAR(p.payment_date),
    SUM(amount),
    AVG(amount)
FROM
    payment AS p
        JOIN
    staff AS s ON p.staff_id = s.staff_id
GROUP BY s.store_id , MONTH(p.payment_date) , YEAR(p.payment_date);

