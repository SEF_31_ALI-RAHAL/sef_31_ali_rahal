SELECT 
    language.name
FROM
    film AS F
        INNER JOIN
    language ON F.language_id = language.language_id
ORDER BY F.language_id
LIMIT 3;