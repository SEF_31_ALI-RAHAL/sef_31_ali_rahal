SELECT 
    Cat.name, COUNT(FC.film_id) AS films
FROM
    category AS Cat
        JOIN
    film_category AS FC ON Cat.category_id = FC.category_id
GROUP BY Cat.name
HAVING COUNT(FC.film_id) BETWEEN 55 AND 65
ORDER BY COUNT(FC.film_id) DESC;