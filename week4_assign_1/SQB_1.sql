SELECT 
    film_actor.actor_id, COUNT(film_actor.film_id)
FROM
    film_actor
        INNER JOIN
    actor ON film_actor.actor_id = actor.actor_id
        INNER JOIN
    film ON film_actor.film_id = film.film_id
GROUP BY film_actor.actor_id;



