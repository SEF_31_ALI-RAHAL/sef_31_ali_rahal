@extends('layouts.app')

@section('content')
<div class="container">

	<form method="POST" action="{{ route('create-item') }}" autocomplete="off">

		@if(count($errors))

			<div class="alert alert-danger">

				<strong>Whoops!</strong> There were some problems with your input.

				<br/>

				<ul>

					@foreach($errors->all() as $error)

					<li>{{ $error }}</li>

					@endforeach

				</ul>

			</div>

		@endif


		<input type="hidden" name="_token" value="{{ csrf_token() }}">


		<div class="row">


			<div class="col-md-6">

				<div class="form-group">

					

				</div>

			</div>

		</div>

	</form>


	<div class="panel panel-primary">

	  <div class="panel-heading">Blender</div>

	  <div class="panel-body">

	    	<form method="GET" action="{{ route('items-lists') }}">


				<div class="row">

					<div class="col-md-6">

						<div class="form-group">

							<input type="text" name="titlesearch" class="form-control" placeholder="Enter Title For Search" value="{{ old('titlesearch') }}">

						</div>

					</div>

					<div class="col-md-6">

						<div class="form-group">

							<button class="btn btn-success">Search</button>

						</div>

					</div>

				</div>

			</form>


			<table class="table table-bordered">

				<thead>

					

					<th>Title</th>

					<th>Creation Date</th>

					<th>Updated Date</th>

				</thead>

				<tbody>

					@if($tweets->count())
						
						@foreach($items as $key => $item)

							<tr>

								

								<td>{{ $item->repo_url }}</td>

								<td>{{ $item->created_at }}</td>

								<td>{{ $item->updated_at }}</td>

							</tr>

						@endforeach
						@foreach($tweets as $key => $item)

							<tr>

								

								<td>{{ $item->tweet_text }}</td>

								<td>{{ $item->created_at }}</td>

								<td>{{ $item->updated_at }}</td>

							</tr>

						@endforeach

					@else

						<tr>

							<td colspan="4">There are no data.</td>

						</tr>

					@endif

				</tbody>

			</table>

			{{ $items->links() }}

	  </div>

	</div>


</div>
@endsection

