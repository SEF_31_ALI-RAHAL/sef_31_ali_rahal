<!-- <html>
   <head>
      <title>Ajax Example</title>
      <meta name="csrf-token" content="{{ csrf_token() }}">
      
      <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>
      
      <script>
         function getMessage(){
           var id = 12; // A random variable for this example

// $.ajax({
//     method: 'GET', // Type of response and matches what we said in the route
//     url: '/getmsg', // This is the url we gave in the route
//     data: {'id' : id}, // a JSON object to send back
//     success: function(response){ // What to do if we succeed
//         console.log(response); 
//     },
//     error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
//         console.log(JSON.stringify(jqXHR));
//         console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
//     }
// });

var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
console.log("a");
$.ajax({
    url: '/getmsg',
    type: 'POST',
    data: {_token: CSRF_TOKEN},
    dataType: 'JSON',
    success: function (data) {
        console.log(data);
    }
});

//   $.get('/test', function(){ 
//         console.log('response'); 
//     });
// $.ajax({
//     method: 'POST',
//     url: 'getmsg/',
// });
         }

      </script>
   </head>
   
   <body>
      <div id = 'msg'>This message will be replaced using Ajax. 
         Click the button to replace the message.</div>
      <?php
      //echo "Ali";
        // echo Form::button('Replace Message',['onClick'=>'getMessage()']);
      ?>
   </body>

</html> -->

<html>
   <head>
      <title>Ajax Example</title>
      
      <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>
      
      <script>
         function getMessage(){
          // console.log("a");
          //   $.ajax({
          //      type:'POST',
          //      url:'http://localhost/Bookmark/public/getmsg',
          //      data:'_token = <?php echo csrf_token() ?>',
          //      success:function(data){
          //         $("#msg").html(data.msg);
          //         console.log("success");
          //      }
          //   });

            var id = 12; // A random variable for this example

$.ajax({
    method: 'POST', // Type of response and matches what we said in the route
    url: 'http://localhost/Bookmark/public/getmsg', // This is the url we gave in the route
    data: {'id' : id}, // a JSON object to send back
    success: function(response){ // What to do if we succeed
        console.log(response); 
    },
    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
        console.log(JSON.stringify(jqXHR));
        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
    }
});

// var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
// console.log("a");
// $.ajax({
//     url: 'http://localhost/Bookmark/public/getmsg',
//     type: 'POST',
//     data: {_token: CSRF_TOKEN},
//     dataType: 'JSON',
//     success: function (data) {
//         console.log(data);
//     }
// });
         }
      </script>
   </head>
   
   <body>
      <div id = 'msg'>This message will be replaced using Ajax. 
         Click the button to replace the message.</div>
      <?php
         echo Form::button('Replace Message',['onClick'=>'getMessage()']);
      ?>
   </body>

</html>