<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Tweet extends Model
{
    //
    use Searchable;
     protected $fillable = ['id','tweet_text','user_id'];

     
    /**

     * Get the index name for the model.

     *

     * @return string

    */

    public function searchableAs()

    {

        return 'items_index';

    }

}
