<?php

namespace App;

use Laravel\Socialite\Contracts\Provider;

class SocialAccountService
{
    public function createOrGetUser(Provider $provider)
    {
        echo 'test';
        //var_dump($provider->user()->token);
        $providerUser = $provider->user();
        
        echo 't';
        $providerName = class_basename($provider); 

        $account = SocialAccount::whereProvider($providerName)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {

            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $providerName,
                'token' => $providerUser->token,
                'tokenSecret'=>$providerUser->tokenSecret,
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {

                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'name' => $providerUser->getName(),

                ]);
            }

            $account->user()->associate($user);
            $account->save();

            return $user;

        }

    }
}

// namespace App;

// use Laravel\Socialite\Contracts\User as ProviderUser;

// class SocialAccountService
// {
//     public function createOrGetUser(ProviderUser $providerUser)
//     {
//         $account = SocialAccount::whereProvider('facebook')
//             ->whereProviderUserId($providerUser->getId())
//             ->first();

//             echo 'hello';

//                //'token' => $providerUser->token
//             $a = $providerUser->token;
//                echo $a;
//         if ($account) {
//             return $account->user;
//         } else {

//             $account = new SocialAccount([
//                 'provider_user_id' => $providerUser->getId(),
//                 'provider' => 'facebook',
//                 'token' => $providerUser->token,

//             ]);

//             $user = User::whereEmail($providerUser->getEmail())->first();

//             if (!$user) {

//                 $user = User::create([
//                     'email' => $providerUser->getEmail(),
//                     'name' => $providerUser->getName(),
//                     'password' => '1234',
//                 ]);
//             }

//             $account->user()->associate($user);
//             $account->save();

//             return $user;

//         }

//     }
// }