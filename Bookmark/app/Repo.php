<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Repo extends Model
{
    //
    use Searchable;
    protected $fillable = ['repo_url','user_id','description','starred_at'];

    public function searchableAs()

    {

        return 'items_index';

    }

    protected $dates = ['created_at', 'updated_at', 'starred_at'];
}
