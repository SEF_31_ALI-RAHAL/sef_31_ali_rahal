<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Contracts\Provider;
use App\SocialAccountService;
use Socialite;
use App\User;
use App\SocialAccount;
class AuthController extends Controller
{
    //
        /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('github')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        try {
            $user = Socialite::driver('github')->user();
        } catch (Exception $e) {
            return Redirect::to('auth/github');
        }

        $authUser = $this->findOrCreateUser($user);

        //Auth::login($authUser, true);
        auth()->login($authUser);

        //return Redirect::to('home');
        return redirect()->to('/home');
    }

    /**
     * Return user if exists; create and return if doesn't
     *
     * @param $githubUser
     * @return User
     */
    private function findOrCreateUser($githubUser)
    {
    	//var_dump($githubUser);
        //6885d88aa989431e4810abe216e866e07f8335f2
        // if ($authUser = User::where('id', $githubUser->id)->first()) {
        //     return $authUser;
        // }
         $account = new SocialAccount([
                'provider_user_id' => $githubUser->getId(),
                'provider' => 'Github',
                'token' => $githubUser->token
            ]);
        $user= User::create([
            'name' => $githubUser->nickname,
            'email' => $githubUser->email,
            'password' => $githubUser ->token
            //'github_id' => $githubUser->id,
            //'avatar' => $githubUser->avatar
        ]);
        $account->user()->associate($user);
            $account->save();

            return $user;
    }
}
