<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AjaxController extends Controller {

   public function index(){
   	//var_dump($request->all());
      $msg = "This is a simple message.";
      var_dump($msg);
      return response()->json(array('msg'=> $msg), 200);
   }
}
