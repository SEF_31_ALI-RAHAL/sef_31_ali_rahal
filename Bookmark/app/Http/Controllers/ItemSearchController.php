<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Requests;

use App\Repo;
use App\Tweet;

class ItemSearchController extends Controller

{


	/**

     * Get the index name for the model.

     *

     * @return string

    */

    public function index(Request $request)

    {

    	if($request->has('titlesearch')){
            
    		$items = Repo::search($request->titlesearch)

    			->paginate(9);
            $tweets = Tweet::search($request->titlesearch)

                ->paginate(9);    

    	}else{

    		$items = Repo::paginate(6);
            $tweets = Tweet::paginate(6);

    	}

    	return view('item-search',compact('items','tweets'));

    }


    /**

     * Get the index name for the model.

     *

     * @return string

    */

    // public function create(Request $request)

    // {

    // 	$this->validate($request,['title'=>'required']);


    // 	$items = Item::create($request->all());

    // 	return back();

    // }

}