<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SocialAccountService;
use Socialite;

class SocialAuthController extends Controller
{
    public function redirect($provider)
    {
        //return Socialite::driver('facebook')->scopes(['user_likes'])->redirect();
        return Socialite::driver($provider)->redirect(); 
    }   

   //  public function callback(SocialAccountService $service)
   //  {
   //      // when facebook call us a with token
   //       $user = $service->createOrGetUser(Socialite::driver('facebook')->stateless()->user());
		 // auth()->login($user);
        

   //      return redirect()->to('/home');  
   //  }
    public function callback(SocialAccountService $service, $provider)
	{
    // Important change from previous post is that I'm now passing
    // whole driver, not only the user. So no more ->user() part
    $user = $service->createOrGetUser(Socialite::driver($provider));

    auth()->login($user);

    return redirect()->to('/home');
	}
}


