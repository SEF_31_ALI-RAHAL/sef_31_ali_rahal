<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' => [
        'client_id' => env('FACEBOOK_CLIENT_ID'),
        'client_secret' => env('FACEBOOK_CLIENT_SECRET'),
        'redirect' => env('FACEBOOK_CALLBACK_URL'),
    ],
    
    'github' => [
        'client_id' => env('GITHUB_CLIENT_ID'),
        'client_secret' => env('GITHUB_CLIENT_SECRET'),
        'redirect' => env('GITHUB_CALLBACK_URL'),
    ],

    'twitter' => [
    'client_id' => 'sLQh1OWnn9zNsiLW4niaKPVmU',
    'client_secret' => 'TY0T88H2dsJXqEVVKeQJ3Xfu0zquH7PlUqci0iCjEC0sDj96Ae',
    //'redirect' => 'http://localhost/Bookmark/public/callback/twitter',
    'redirect' => 'http://localhost/Bookmark/public/auth/twitter/callback',

    'google' => [
    'client_id' => '617175114128-fbiulab3t5khgujllla961nnlpnvhcj1.apps.googleusercontent.com',
    'client_secret' => 'CokE9QlRcs8eoY0OLeN05ktL',
    'redirect' => 'http://127.0.0.1/Bookmark/public/callback/google',
],

],


];
