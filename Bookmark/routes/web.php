<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('ajax',function(){
   return view('message');
});

Route::get('items-lists', ['as'=>'items-lists','uses'=>'ItemSearchController@index']);

Route::post('create-item', ['as'=>'create-item','uses'=>'ItemSearchController@create']);

Route::get('/home', 'HomeController@index');
//Route::get('/redirect', 'SocialAuthController@redirect');
//Route::get('/redirect/{provider}', 'SocialAuthController@redirect');
//Route::get('/callback', 'SocialAuthController@callback');
//Route::get('/callback/{provider}', 'SocialAuthController@callback');


Route::get('auth/twitter', 'testauth@redirectToProvider');
    Route::get('auth/twitter/callback', 'testauth@handleProviderCallback');

Route::get('auth/github', 'AuthController@redirectToProvider');
    Route::get('auth/github/callback', 'AuthController@handleProviderCallback');

//Route::post('/home/search','HomeController@search');
Route::post('/getmsg','AjaxController@index');

Route::get('/test', 'HomeController@testfunction');
Route::post('/test', 'HomeController@testfunction');



// Route::get('/', ['uses' => 'GithubController@index', 'as' => 'index']);

// Route::get('/finder', ['uses' => 'GithubController@finder', 'as' => 'finder']);

// Route::get('/edit', ['uses' => 'GithubController@edit', 'as' => 'edit_file']);

// Route::post('/update', ['uses' => 'GithubController@update', 'as' => 'update_file']);

// Route::get('/commits', ['uses' => 'GithubController@commits', 'as' => 'commits']);