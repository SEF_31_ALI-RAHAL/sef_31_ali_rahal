CREATE TABLE `ClaimsPatients`.`Claims` (
  `claim_id` INT NOT NULL,
  `patiend_id` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`claim_id`));

ALTER TABLE `ClaimsPatients`.`Claims` 
DROP PRIMARY KEY,
ADD PRIMARY KEY (`claim_id`, `patiend_name`);


INSERT INTO `ClaimsPatients`.`Claims` (`claim_id`, `patiend_id`) VALUES ('1', 'Bassem Dghaidi');
INSERT INTO `ClaimsPatients`.`Claims` (`claim_id`, `patiend_id`) VALUES ('2', 'Omar Breidi');
INSERT INTO `ClaimsPatients`.`Claims` (`claim_id`, `patiend_id`) VALUES ('3', 'Marwan Sawwan');


CREATE TABLE `ClaimsPatients`.`Defendants` (
  `claim_id` INT NOT NULL,
  `defendant_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`claim_id`, `defendant_name`));

INSERT INTO `ClaimsPatients`.`Defendants` (`claim_id`, `defendant_name`) VALUES ('1', 'Jean skaff');
INSERT INTO `ClaimsPatients`.`Defendants` (`claim_id`, `defendant_name`) VALUES ('1', 'Elie Meouchi');
INSERT INTO `ClaimsPatients`.`Defendants` (`claim_id`, `defendant_name`) VALUES ('1', 'Radwan Sameh');
INSERT INTO `ClaimsPatients`.`Defendants` (`claim_id`, `defendant_name`) VALUES ('2', 'Joesph Eid');
INSERT INTO `ClaimsPatients`.`Defendants` (`claim_id`, `defendant_name`) VALUES ('2', 'Paul Syoufi');
INSERT INTO `ClaimsPatients`.`Defendants` (`claim_id`, `defendant_name`) VALUES ('2', 'Radwan Sameh');
INSERT INTO `ClaimsPatients`.`Defendants` (`claim_id`, `defendant_name`) VALUES ('3', 'Issam Awwad');




CREATE TABLE `ClaimsPatients`.`LegalEvents` (
  `claim_id` INT NOT NULL,
  `defendant_name` VARCHAR(45) NOT NULL,
  `claim_status` VARCHAR(45) NOT NULL,
  `change_date` DATE NULL,
  PRIMARY KEY (`claim_id`, `defendant_name`, `claim_status`));

INSERT INTO `ClaimsPatients`.`LegalEvents` (`claim_id`, `defendent_name`, `claim_status`, `change_date`) VALUES ('1', 'Jean Skaff', 'AP', '2016-01-01');
INSERT INTO `ClaimsPatients`.`LegalEvents` (`claim_id`, `defendent_name`, `claim_status`, `change_date`) VALUES ('1', 'Jean Skaff', 'OR', '2016-02-02');
INSERT INTO `ClaimsPatients`.`LegalEvents` (`claim_id`, `defendent_name`, `claim_status`, `change_date`) VALUES ('1', 'Jean Skaff', 'SF', '2016-03-01');
INSERT INTO `ClaimsPatients`.`LegalEvents` (`claim_id`, `defendent_name`, `claim_status`, `change_date`) VALUES ('1', 'Jean Skaff', 'CL', '2016-04-01');
INSERT INTO `ClaimsPatients`.`LegalEvents` (`claim_id`, `defendent_name`, `claim_status`, `change_date`) VALUES ('1', 'Radwan Sameh', 'AP', '2016-01-01');
INSERT INTO `ClaimsPatients`.`LegalEvents` (`claim_id`, `defendent_name`, `claim_status`, `change_date`) VALUES ('1', 'Radwan Sameh', 'OR', '2016-02-02');
INSERT INTO `ClaimsPatients`.`LegalEvents` (`claim_id`, `defendent_name`, `claim_status`, `change_date`) VALUES ('1', 'Radwan Sameh', 'SF', '2016-03-01');
INSERT INTO `ClaimsPatients`.`LegalEvents` (`claim_id`, `defendent_name`, `claim_status`, `change_date`) VALUES ('1', 'Elie Meouchi', 'AP', '2016-01-01');
INSERT INTO `ClaimsPatients`.`LegalEvents` (`claim_id`, `defendent_name`, `claim_status`, `change_date`) VALUES ('1', 'Elie Meouchi', 'OR', '2016-02-02');
INSERT INTO `ClaimsPatients`.`LegalEvents` (`claim_id`, `defendent_name`, `claim_status`, `change_date`) VALUES ('2', 'Radwan Sameh', 'AP', '2016-01-01');
INSERT INTO `ClaimsPatients`.`LegalEvents` (`claim_id`, `defendent_name`, `claim_status`, `change_date`) VALUES ('2', 'Radwan Sameh', 'OR', '2016-02-01');
INSERT INTO `ClaimsPatients`.`LegalEvents` (`claim_id`, `defendent_name`, `claim_status`, `change_date`) VALUES ('2', 'Paul Syoufi', 'AP', '2016-01-01');
INSERT INTO `ClaimsPatients`.`LegalEvents` (`claim_id`, `defendent_name`, `claim_status`, `change_date`) VALUES ('3', 'Issam Awwad', 'AP', '2016-01-01');



CREATE TABLE `ClaimsPatients`.`ClaimStatusCodes` (
  `claim_status` INT NOT NULL,
  `claim_status_desc` VARCHAR(45) NULL,
  `claim_req` INT NULL,
  PRIMARY KEY (`claim_status`));

  ALTER TABLE `ClaimsPatients`.`ClaimStatusCodes` 
CHANGE COLUMN `claim_status` `claim_status` VARCHAR(45) NOT NULL ;


  INSERT INTO `ClaimsPatients`.`ClaimStatusCodes` (`claim_status`, `claim_status_desc`, `claim_req`) VALUES ('AP', 'Awaiting review panel', '1');
INSERT INTO `ClaimsPatients`.`ClaimStatusCodes` (`claim_status`, `claim_status_desc`, `claim_req`) VALUES ('OR', 'Panel opinion rendered', '2');
INSERT INTO `ClaimsPatients`.`ClaimStatusCodes` (`claim_status`, `claim_status_desc`, `claim_req`) VALUES ('SF', 'Suit filed', '3');
INSERT INTO `ClaimsPatients`.`ClaimStatusCodes` (`claim_status`, `claim_status_desc`, `claim_req`) VALUES ('CL', 'Closed', '4');


SELECT 
    MinCount.claim_id, patiend_name, claim_status
FROM
    (SELECT 
        claim_id, MIN(countstatus) AS MinimumStatus
    FROM
        (SELECT 
        claim_id, defendent_name, COUNT(claim_status) AS countstatus
    FROM
        LegalEvents
    GROUP BY claim_id , defendent_name) AS Result
    GROUP BY claim_id) AS MinCount
        JOIN
    ClaimStatusCodes ON MinimumStatus = ClaimStatusCodes.claim_req
        JOIN
    Claims ON Claims.claim_id = MinCount.claim_id
ORDER BY claim_id;


