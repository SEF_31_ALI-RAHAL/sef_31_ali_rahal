<?php
//using shuffle randomizes the order of elements

function get_random_elements( $array,$limit = 0 ) {
   
    shuffle($array);

    if ( $limit > 0 ) {
        $array = array_splice($array, 0, $limit);
    }
    return $array;
}

$input = array(25,50,75,100);

$s=get_random_elements($input , $limit=0);
print_r($s);
?>