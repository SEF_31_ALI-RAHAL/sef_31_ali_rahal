function get_titles() {
    var titles = new Array;
    var titles_str = localStorage.getItem('title');
    if (titles_str !== null) {
        titles = JSON.parse(titles_str); 
    }
    return titles;
}   
function get_description() {
    var desc = new Array;
    var desc_str = localStorage.getItem('descrption');
    if (desc_str !== null) {
        desc = JSON.parse(desc_str); 
    }
    return desc;
}
function add() {
    var task = document.getElementById('myInput').value;
    var descrption = document.getElementById('desc').value;
    var titles = get_titles();
    var descrptionArray = get_description();
    titles.push(task);
    descrptionArray.push(descrption);
    localStorage.setItem('title', JSON.stringify(titles));
    
    localStorage.setItem('descrption', JSON.stringify(descrptionArray));
    show();
}
function show() {
    var titles = get_titles();
    var desc = get_description();
    for(var i=0; i<titles.length; i++)
    {
        adddom(titles[i],desc[i],i);
    }         
}

function adddom(title,desc,buttonid){
    var ColumnData = document.createElement("div");
    var titleheader = document.createElement("h1");
    var descrption = document.createElement("h3");
    var input = document.createTextNode(title);
    var desc = document.createTextNode(desc);
    descrption.appendChild(desc);
    titleheader.appendChild(input);
    ColumnData.appendChild(titleheader);
    ColumnData.appendChild(descrption);
    ColumnData.className="ColumnData";
    var ColumnClose = document.createElement("div");
    var closecontainer = document.createElement("div");
    closecontainer.className = "closecontainer";
    var button = document.createElement("button");
    var t = document.createTextNode("x");
    button.appendChild(t);
    button.className="closebutton";
    button.id=buttonid;
    button.setAttribute('onclick','remove();');
    ColumnClose.className = "ColumnClose";
    ColumnClose.appendChild(closecontainer);
    closecontainer.appendChild(button);
    var note = document.createElement("div");
    note.className="notes";
    note.appendChild(ColumnData);
    note.appendChild(ColumnClose);
    document.getElementById("container").appendChild(note);          
}

function remove() 
{
    var close = document.getElementsByClassName("closebutton");
    for (var i = 0; i < close.length; i++) 
    {
         close[i].onclick = function() 
         {
             var closecontainer = this.parentElement;
             var ColumnClose = closecontainer.parentElement;
             var note = ColumnClose.parentElement;
             note.style.display = "none";
             var id = this.getAttribute('id');
             var titles = get_titles();
             var descrption = get_description();
             titles.splice(id, 1);
             descrption.splice(id,1);
             localStorage.setItem('title', JSON.stringify(titles));
             localStorage.setItem('descrption', JSON.stringify(descrption));   
         }
    }
}
